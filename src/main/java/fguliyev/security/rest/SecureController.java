package fguliyev.security.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@Slf4j
@RestController
@RequestMapping("/secure")
public class SecureController {

    @GetMapping("/hello")
    public ResponseEntity<HelloDto> helloWorld() {
        log.info("Hello method in action");
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("response", "This is a demo header");
        return new ResponseEntity<HelloDto>(new HelloDto("Welcome user, for login please."), responseHeaders, HttpStatus.CREATED);
    }

    @GetMapping("/authenticated")
    public HelloDto helloWorld(Principal principal) {
        log.info("Authenticated method in action");
        return new HelloDto("Welcome " + principal.getName());
    }

    //USER
    @GetMapping("/user")
    public HelloDto helloUser(Principal principal) {
        return new HelloDto("Welcome  " + principal.getName() + ". This is a secure content, that is accessible only to USER rule.");
    }

    //ADMIN
    @GetMapping("/admin")
    public HelloDto helloAdmin(Principal principal) {
        return new HelloDto("Welcome  " + principal.getName() + ". This is a secure content, that is accessible only to ADMIN rule.");
    }

    @PostMapping("/post")
    public HelloDto adminPostExample(@RequestBody HelloDto helloDto, Principal principal) {
        log.info("The user is {}", principal);
        return helloDto;
    }

    @GetMapping("/post")
    public String adminPostExample(Principal principal) {
        return "Hello " + principal.getName();
    }
}
