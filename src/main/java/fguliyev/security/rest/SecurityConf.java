package fguliyev.security.rest;

import fguliyev.security.config.FilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true
)
public class SecurityConf extends WebSecurityConfigurerAdapter {

    private final FilterConfigurerAdapter filterConfigurerAdapter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.apply(filterConfigurerAdapter);
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/secure/hello/**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/secure/user").hasAnyRole("USER", "ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/secure/admin").hasAnyRole("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/secure/post").authenticated()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/secure/post").authenticated();

        super.configure(http);
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("fguliyev")
//                .password("{noop}fguliyev")
//                .roles("USER")
//                .and()
//                .withUser("admin")
//                .password("{noop}admin")
//                .roles("ADMIN");
//    }
}
