package fguliyev.security.jwt;

import fguliyev.security.domain.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class JwtService {

    private Key key;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode("and0IGlzIGNvb2wgQmFzZTY0IEJhc2U2NCBCYXNlNjQgQmFzZTY0IEJhc2U2NCBCYXNlNjQgQmFzZTY0IEJhc2U2NCBCYXNlNjQ");
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String issueToken(User user, Duration duration) {
        log.trace("Issue JWT token to {} for {}", user);
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .addClaims(Map.of("ROLE", List.of("ADMIN", "STUDENT", "MANAGER")));
        return jwtBuilder.compact();
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

}