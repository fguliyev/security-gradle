package fguliyev.security;

import fguliyev.security.domain.User;
import fguliyev.security.domain.UserAuthority;
import fguliyev.security.jwt.JwtService;
import fguliyev.security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.Duration;
import java.util.Set;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class        SecurityApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final JwtService jwtService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Creating demo users");
        User user = new User();
        user.setUsername("fguliyev");
        user.setPassword(passwordEncoder().encode("12345"));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);

        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setAuthority("ROLE_ADMIN");

        user.setAuthorities(Set.of(userAuthority));

        userRepository.save(user);

        String jwt = jwtService.issueToken(user, Duration.ofDays(1));
        log.info("Jwt is : {}", jwt);

        log.info("Claims from jwt : {}", jwtService.parseToken(jwt));
    }
}
