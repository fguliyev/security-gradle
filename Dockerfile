FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/security-1.0.c40d903.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/security-1.0.c40d903.jar"]
